define BUILD_PUSH_FOLDER
	DOCKER_FILE=$(3); \
	DOCKER_FILE=$${DOCKER_FILE:-Dockerfile}; \
	cd $(1) && docker build --no-cache -f $${DOCKER_FILE} -t $(2) . && docker push $(2)
endef


.PHONY: docker
docker:
	$(call BUILD_PUSH_FOLDER,docker,yen3/docker)

.PHONY: ubuntu-man
ubuntu-man:
	$(call BUILD_PUSH_FOLDER,ubuntu-man,yen3/ubuntu-man:18.04)

.PHONY: go
go:
	$(call BUILD_PUSH_FOLDER,go,yen3/golang:1.12.3-alpine3.9)
	$(call BUILD_PUSH_FOLDER,go,yen3/golang:latest)

.PHONY: pandoc
pandoc:
	$(call BUILD_PUSH_FOLDER,pandoc,yen3/pandoc:latest)
	$(call BUILD_PUSH_FOLDER,pandoc,yen3/pandoc:2.7.3)

.PHONY: pyenv-tox
pyenv-tox:
	$(call BUILD_PUSH_FOLDER,pyenv-tox,yen3/pyenv-tox:latest)

.PHONY: compiler-course
compiler-course:
	$(call BUILD_PUSH_FOLDER,compiler-course,yen3/compiler-course:latest)

.PHONY: singularity
singularity:
	$(call BUILD_PUSH_FOLDER,singularity,yen3/singularity:latest)

.PHONY: hoogle
hoogle:
	$(call BUILD_PUSH_FOLDER,hoogle,yen3/hoogle)

.PHONY: haskell-stack
haskell-stack:
	$(call BUILD_PUSH_FOLDER,haskell-stack,yen3/haskell-stack)
