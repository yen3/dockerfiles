# Memo

## Simple example

```sh
make all
docker run -it --rm -p 8000:80 yen3/woboq_codebrowser_bear
# Visit http://localhost:8000/codebrowser/bear
```


```sh
make kernel # wait for a long time
docker run -it --rm -p 8000:80 yen3/woboq_codebrowser_kernel 
# Press Ctrl-C to stop the server
# Visit http://localhost:8000/codebrowser/kernel
```
