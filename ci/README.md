# Self-host gitea + drone

* The docker-compose can run in mac OSX 10.13 with Docker for mac 18.03
* It use dind to resolve the problem that drone can not find proper url to
  clone git repo.


## Port mapping

* `localhost:10080`: gitea web (`3000`)
* `localhost:10022`: gitea ssh (`22`)
* `localhost:10090`: drone web (`8000`)
* `localhost:10900`: drone server (`9000`)

## Before run the servers

* You have to modify fields which is formed with `<...>` in `data/ci/docker-compose.yml`
  e.g. `<org>`, `<admin-account>`, `<server-secret>` and etc ...

## Start the servers

* Command memo

    ```
    docker-compose up -d
    ```

## Troubleshooting (Workaround)

* After enabled drone ci job for gitea's repo, the original webhook addr in repo
  does not work. You have to change it to drone's internal web addr manaually.
    * Example:
        * Original: `http://localhost:10090`
        * Modified: `http://172.27.2.4:8000`
