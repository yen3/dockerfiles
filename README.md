# Docker images for persnal usage

## Common code snippet

* Remove cache for debian/ubuntu image

```sh
  && apt-get clean all \
  && rm -rf \
         /var/cache/debconf/* \
         /var/lib/apt/lists/* \
         /var/log/* \
         /tmp/* \
         /var/tmp/*
```

* Set dockerfile filetype in vim

```
# vim: set ft=dockerfile:
```
