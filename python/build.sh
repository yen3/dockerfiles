#!/bin/bash

IMAGE_NAME=yen3/python
PUSH_IMAGE_NAME=${IMAGE_NAME}

docker build --rm -t ${IMAGE_NAME}:ubuntu -f Dockerfile.ubuntu .
docker build --rm -t ${IMAGE_NAME}:latest .

if [ x"$1" == x"push" ]; then
    docker tag ${IMAGE_NAME}:ubuntu ${PUSH_IMAGE_NAME}:ubuntu
    docker tag ${IMAGE_NAME}:latest ${PUSH_IMAGE_NAME}:latest
    docker push ${PUSH_IMAGE_NAME}:ubuntu
    docker push ${PUSH_IMAGE_NAME}:latest
fi
