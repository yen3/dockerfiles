# Compiler course by Alex Aiken

- Pull the docer image

  ```sh
  docker pull yen3/compiler-course:latest
  ```

- Course link: https://courses.edx.org/courses/course-v1:StanfordOnline+SOE.YCSCS1+1T2020/course/
- The course suggests learners to use compiler-vm. It's too heavy for me.
- If you are familiar with docker. The environment can be constructed by
  docker
