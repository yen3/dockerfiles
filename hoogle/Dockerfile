FROM haskell:8.8.4 as builder

RUN set -ex \
    && apt update \
    && apt upgrade -y \
    && apt install -y git \
    && git clone https://github.com/ndmitchell/hoogle \
    && cd hoogle \
    && cabal update \
    && cabal install \
    && /root/.cabal/bin/hoogle generate


FROM debian:buster

ENV PATH "/root/.cabal/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

COPY --from=builder /root/.cabal /root/.cabal
COPY --from=builder /root/.hoogle /root/.hoogle

EXPOSE 8080

ENTRYPOINT ["/root/.cabal/bin/hoogle"]

CMD ["server"]
