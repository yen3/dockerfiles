#!/bin/bash

docker-compose up -d
sleep 10

docker-compose exec hadoop bash -xc "hdfs dfs -rm -r /input || true"
docker-compose exec hadoop bash -xc "hdfs dfs -rm -r /output || true"
docker-compose exec hadoop bash -xc "hdfs dfs -mkdir /input && hdfs dfs -put etc/hadoop/*.xml /input && hadoop jar /hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.5.jar grep /input /output 'dfs[a-z.]+' && hdfs dfs -get /output /output && cat /output/*"

docker-compose down
