#!/bin/bash

/usr/sbin/sshd

hdfs namenode -format

/hadoop/sbin/start-dfs.sh
/hadoop/sbin/start-yarn.sh

tail -f /dev/null
