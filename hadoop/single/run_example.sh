#!/bin/bash

mkdir -p input
cp /hadoop/etc/hadoop/*.xml input
hadoop jar /hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.5.jar grep input output 'dfs[a-z.]+'
cat output/*
